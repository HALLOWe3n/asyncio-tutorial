import os
import asyncio

from urllib.request import urlopen


async def coroutine(url):
    request = urlopen(url)
    file_name = os.path.basename((url))

    with open(file_name, 'wb') as file:
        while True:
            chuck = request.read(1024)
            if not chuck:
                break
            file.write(chuck)
        message = '{} download successfuly '.format(file_name)
        return message


async def main(urls):
    # create list of coroutines and waiting for their completion
    coroutines = [coroutine(url) for url in urls]
    finish, pending = await asyncio.wait(coroutines)
    for item in finish:
        print(item.result())


if __name__ == '__main__':
    urls = [
        "http://www.irs.gov/pub/irs-pdf/f1040.pdf",
        "http://www.irs.gov/pub/irs-pdf/f1040a.pdf",
        "http://www.irs.gov/pub/irs-pdf/f1040ez.pdf",
        "http://www.irs.gov/pub/irs-pdf/f1040es.pdf",
        "http://www.irs.gov/pub/irs-pdf/f1040sb.pdf"
    ]

    event_loop = asyncio.get_event_loop()
    try:
        event_loop.run_until_complete(main(urls))
    finally:
        event_loop.close()
