import asyncio


async def task(seconds):
    print('Задание выполняется в течении {} секунд'.format(seconds))
    await asyncio.sleep(seconds)
    return 'Задание завершено'

if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    try:
        print('Начало выполнения задания')
        obj_task = event_loop.create_task(task(seconds=5))
        event_loop.run_until_complete(obj_task)
    finally:
        event_loop.close()
    print('Результат выполнения задания: {}'.format(obj_task.result()))
