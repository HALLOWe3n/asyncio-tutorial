import socket

# Определение сервер сокета
# socket.AF_INET - ip протокол 4 уровня
# socket.SOCK_STREAM - поддержка протокола TCP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Определение переиспользования порта и адреса сокета при потере соединения (убрать TIMEOUT)
# SO_REUSEADDR - константа переиспользования адреса
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Привязать сокет
# функция bind принимает кортеж
server_socket.bind(('localhost', 5000))

# Прослушивать сокетом буфер на предмет входяших подключений
server_socket.listen()


while True:
    print('before accept')

    # Определение клиентского сокета
    # accept - функция прослушивания буфера
    # accept - блокирующая функция, которая ожидает соеденение со стороны клиета
    client_socket, addr = server_socket.accept()
    print(f'connection addr: {addr}')

    # Ожидание получения сообщения от клиента
    while True:
        print('before recv')

        # сообщение пользователя
        request = client_socket.recv(4096)
        if not request:
            break

        # возвращение от сервера клиенту
        # encode() кодирует в bytes без передачи аргументов по-умолчанию
        response = 'Hello World\n'.encode()
        # тут думаю все ясно
        client_socket.send(response)

    print('Outside inner while loop')
    # закрываем клиентский сокет
    # если не закрыть сервер может подумать что мы медленно отвечаем
    client_socket.close()


# Резюме:
# В данном примере совершенно нет ассинхронности
# Главная проблема этого примера - здесь совершенно нет возможности подключиться другому пользователю
# так как соединение будет отбирать первый клиент
# решение - event loop который будет давать возможность подключаться другим пользователям в момент пока предидущий
# пользователь 'думает'
