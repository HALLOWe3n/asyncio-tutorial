import asyncio


async def foo():
    print('running in foo')
    await asyncio.sleep(1)
    print('Explicit context switch to foo again')


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    event_loop.run_until_complete(foo())
    event_loop.close()
