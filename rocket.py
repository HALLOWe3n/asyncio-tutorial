import time


def rocker_start(delay, countdown):
    time.sleep(delay)
    for i in reversed(range(countdown)):
        print(f'{i + 1}...')
        time.sleep(1)
    print('Falcon Heavy launched!')


if __name__ == '__main__':
    rocker_start(2, 10)
